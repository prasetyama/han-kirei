@extends('layouts.app')

@section('content')
<div class="container-fluid">

        <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Produk</h1>
    <div class="mb-1">
        <a href="{{ route('product.create') }}" class="btn btn-outline-primary">
            <i class="fa fa-plus"></i> Tambah Produk Baru
        </a>
    </div>
    @if (Session::has('message'))
        <div class="alert alert-success">
            {!! session('message') !!}
        </div>
    @endif
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Produk Lis</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama Produk</th>
                <th>Deskripsi Singkat</th>
                <th>Deskripsi Lengkap</th>
                <th>Image</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($products as $key => $product)
                  <tr>
                      <td>{{ $key+1 }}</td>
                      <td>{{ $product->product_name }}</td>
                      <td>{{ $product->short_description }}</td>
                      <td>{!! $product->long_description !!}</td>
                      <td>
                          <img src="{{ url($product->thumbnail) }}" alt="{{$product->thumbnail}}" class="img-fluid" width="80">
                      </td>
                      <td>
                        <a href="{{ route('product.update', encrypt($product->id)) }}" class="btn btn-primary btn-circle btn-sm">
                              <i class="fa fa-edit"></i>
                          </a>
                          <button data-toggle="modal" data-target="#deleteProduct{{ $product->id }}" class="btn btn-danger btn-circle btn-sm">
                              <i class="fa fa-trash"></i>
                          </button>
                      </td>
                  </tr>
                  <div class="modal fade" id="deleteProduct{{ $product->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">{{ $product->product_name }}</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          Apakah anda yakin ingin menghapus product <strong>{{ $product->product_name }}</strong> ?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <a href="{{ route('product.destroy', encrypt($product->id)) }}" type="button" class="btn btn-primary">Ya, Hapus</a>
                        </div>
                      </div>
                    </div>
                  </div>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
@endsection