@extends('layouts.app')

@section('content')
<div class="container-fluid">

        <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Product</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Tambah Produk</h6>
      </div>
      <div class="card-body">
        <form action="{{ route('product.create.proses') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('product_name') ? 'has-error' : ''}}">
                        <label>Nama Produk</label>
                        <input type="text" name="product_name" class="form-control">
                        <strong class="text-danger">{!! $errors->first('product_name', '<p class="help-block">:message</p>') !!}</strong>
                    </div>
                    <div class="form-group{{ $errors->has('short_description') ? 'has-error' : ''}}">
                        <label>Deskripsi Singkat</label>
                        <input type="text" name="short_description" class="form-control">
                        <strong class="text-danger">{!! $errors->first('short_description', '<p class="help-block">:message</p>') !!}</strong>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('thumbnail') ? 'has-error' : ''}}">
                        <img id="img-preview" alt="your image" width="50%" class="img-responsive" src="{{ url('/admin/img/no-image.webp') }}" />
                        <input type="file" class="form-control" name="thumbnail" onchange="document.getElementById('img-preview').src = window.URL.createObjectURL(this.files[0])">
                        <strong class="text-danger">{!! $errors->first('thumbnail', '<p class="help-block">:message</p>') !!}</strong>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group{{ $errors->has('long_description') ? 'has-error' : ''}}">
                        <label>Deskripsi Lengkap</label>
                        <textarea name="long_description" class="ckeditor form-control" cols="3"></textarea>
                        <strong class="text-danger">{!! $errors->first('long_description', '<p class="help-block">:message</p>') !!}</strong>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-outline-success">Submit</button>
        </form>
      </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection