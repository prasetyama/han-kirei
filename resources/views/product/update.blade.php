@extends('layouts.app')

@section('content')
<div class="container-fluid">

        <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Product</h1>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Ubah Produk</h6>
      </div>
      <div class="card-body">
        <form action="{{ route('product.update.proses') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-6">
                    <input type="hidden" name="id" value="{{ $product->id }}">
                    <div class="form-group">
                        <label>Nama Produk</label>
                        <input type="text" name="product_name" value="{{ $product->product_name }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label>Deskripsi Singkat</label>
                        <input type="text" name="short_description" value="{{ $product->short_description }}" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ url($product->thumbnail) }}" id="img-preview" style="margin-bottom: 10px" alt="" width="300px" class="img-responsive">
                                <input type="file" class="form-control" name="thumbnail" onchange="document.getElementById('img-preview').src = window.URL.createObjectURL(this.files[0])">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Deskripsi Lengkap</label>
                        <textarea name="long_description" class="ckeditor form-control" cols="3">{{ $product->long_description }}</textarea>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-outline-success">Update</button>
        </form>
      </div>
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.ckeditor').ckeditor();
    });
</script>
@endsection