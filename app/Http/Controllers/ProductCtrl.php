<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;

// declare model
use App\Product;

class ProductCtrl extends Controller
{
    public function index()
    {
        $products = Product::all();
        return view('product.index', compact('products'));
    }

    public function create()
    {
        return view('product.create');
    }

    public function create_proses(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'thumbnail' => 'required',
        ]);
        $product = new Product;
        $product->product_name = $request->product_name;
        $product->short_description = $request->short_description;
        $product->long_description = $request->long_description;
        $product->thumbnail = $request->file('thumbnail')->store('uploads/product');
        if ($product->save()) {
            $request->session()->flash('message', 'Data berhasil tersimpan!');
            return redirect('product');
        }
    }

    public function update($id)
    {
        $product = Product::where('id', decrypt($id))->first();
        return view('product.update', compact('product'));
    }

    public function update_proses(Request $request)
    {
        $product = Product::where('id', $request->id)->first();
        if ($product !== null) {
            $product->product_name = $request->product_name;
            $product->short_description = $request->short_description;
            $product->long_description = $request->long_description;

            if ($request->file('thumbnail')!=null){
                $product->thumbnail = $request->file('thumbnail')->store('uploads/product');
            }
            if ($product->save()) {
                $request->session()->flash('message', 'Data berhasil terubah!');
                return redirect('product');
            }
        }
    }

    public function destroy(Request $request, $id)
    {
        $product = Product::where('id', decrypt($id))->first();
        if ($product !== null) {
            if (Product::destroy(decrypt($id))) {
                if ($product->thumbnail !== null) {
                    $logo = public_path().'/'.$product->thumbnail;
                    if (file_exists($logo)) {
                        unlink($logo);
                    }
                }
                $request->session()->flash('message', 'Data berhasil terhapus!');
                return redirect('product');
            }
        }
    }
}
