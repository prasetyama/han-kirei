<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    protected $table = 'product_gallery';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
