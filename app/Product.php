<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public $timestamps = true;

    public function gallery()
    {
        return $this->hasMany(ProductGallery::class);
    }
}
